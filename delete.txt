FILES THAT COULD BE DELETED AT 04.05.2021:

>impressum.html
>dienstleistungen.html
>projects.html
>ueberuns.html

WHY?
I added new directories to this project to get better URLs

Example:
https://mj-partners.com/ueberuns.html (before)
https://mj-partners.com/über (after)

https://mj-partners.com/impressum.html (before)
https://mj-partners.com/impressum (after)

So I added folders for every undersite and get the HTML from an 'index.php' file so the original files are not longer necessary.
- Changed by Jorit Vásconez Gerlach
- Changed on 2021.04.04

Questions?
📧 jorit@vasconezgerlach
📞 +49 176 56928667
🌐 jorit.vasconezgerlach.de